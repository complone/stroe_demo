﻿using System.Threading.Tasks;
using BootstrapTest.Models;

namespace BootstrapTest.Manager
{
    public interface ICustomUserStore
    {
        Task CreateAsync(CustomUser user);
        Task DeleteAsync(CustomUser user);
        void Dispose();
        Task<CustomUser> FindByIdAsync(string userId);
        Task<CustomUser> FindByNameAsync(string userName);
        Task UpdateAsync(CustomUser user);
    }
}