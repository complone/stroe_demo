﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BootstrapTest.Dao
{
    public interface IUser<out Ikey>
    {
        //
        // Summary:
        //     Unique key for the user
        Ikey Id { get; }

        //
        // Summary:
        //     Unique username
        string UserName { get; set; }

    }
}