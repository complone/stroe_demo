﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BootstrapTest.Models
{
    public class Movie
    {

       

        public int ID { get; set; }




        [StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }




        [Display(Name = "ReleeaseDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReleeaseDate { get; set; }




        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Required]
        [StringLength(30)]
        public string Genre { get; set; }




        [Range(1, 100)]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }


        public class MovieDBContext : DbContext
        {
            public DbSet<Movie> Movies { get; set; }
        }



    }
}