﻿using BootstrapTest.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BootstrapTest.Models
{
    public class CustomUser : IUser<string>
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}