﻿using BootstrapTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BootstrapTest.Controllers
{
    public class HelloWorldController : Controller
    {
        private BootstrapTestContext db = new BootstrapTestContext();

        //
        // GET: /HelloWorld/
        public ActionResult Index(string id) {
            string searchPriceIndex = id;
            //var Prices = from n in db.Movies
            //             orderby n ascending
            //             select n;
          

            return View();
        }

        //
        //GET: /HelloWorld/Welcome



        public ActionResult Welcome(string name, int numTimes = 1) {
            ViewBag.Message = "Hello" + name;
            ViewBag.NumTimes = numTimes;
            return View();
        }

        //
        //GET: /HelloWorld/ParseTest

        public string ParseTest(string name, int ID = 1) {
            return HttpUtility.HtmlEncode("Hello " + name + ", ID=" + ID);
        }
    }
}