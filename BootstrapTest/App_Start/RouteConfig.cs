﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BootstrapTest
{
    public class RouteConfig
    {
        //当您运行应用程序并且不提供任何URL段时，
        //它默认为“Home”控制器和上述代码的默认部分中指定的“Index”操作方法。
        //URL的第一部分决定要执行的控制器类。所以/ HelloWorld映射到HelloWorldController类。
        //URL的第二部分确定要执行的类的操作方法。
        //所以/ HelloWorld / Index会导致类的Index方法HelloWorldController执行。
        //请注意，我们只需要浏览到/ HelloWorld，Index默认情况下使用该方法。
        //这是因为一个名为Index的方法是在控制器上调用的默认方法，如果未明确指定。
        //URL段（Parameters）的第三部分用于路由数据。稍后我们将在本教程中看到路线数据。
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name:"Hello",
                url:"{controller}/{action}/{name}/{id}"
            );
            routes.MapRoute(
                name:"select",
                url: "{controller}/{action}/{count}/{Cenres}/{id}",
                defaults:new { controller = "HelloWorld",action = "Index"}
            );
        }
    }
}
